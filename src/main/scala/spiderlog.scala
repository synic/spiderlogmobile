package com.synicworld.spiderlog

import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookieStore
import java.net.CookiePolicy

import android.view.Menu
import android.view.MenuItem
import android.view.MenuInflater
import android.app.Activity
import android.os.Bundle
import android.content.Intent
import android.support.v4.app.FragmentActivity
import android.view.Window
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.content.Context
import android.util.Log

import com.synicworld.spiderlog.api.ApiResponse
import com.synicworld.spiderlog.api.UserInfoTask

object SpiderLog {
  val ACTIVITY_LOGIN = 0

  val ACTION_LOGOUT = "spiderlog:logout"
}

class SpiderLog extends FragmentActivity {
  // constants
  val TAG = "SpiderLog"
  lazy val jar = new CookieJar(this)

  // init the api
  Api.init()

  override def onCreate(bundle: Bundle) {
    super.onCreate(bundle)
    requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS)
    setContentView(R.layout.main)

    CookieHandler.setDefault(new CookieManager(
      jar, CookiePolicy.ACCEPT_ORIGINAL_SERVER))

    registerReceiver(logoutReceiver, new IntentFilter(SpiderLog.ACTION_LOGOUT))
    testLogin
  }

  override def onCreateOptionsMenu(menu: Menu) = {
    new MenuInflater(getApplication()).inflate(
      R.menu.main_options, menu)
    super.onCreateOptionsMenu(menu)
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId() match {
      case R.id.logout => {
        sendBroadcast(new Intent(SpiderLog.ACTION_LOGOUT))
        true
      }
    }

    return super.onOptionsItemSelected(item)
  }

  def testLogin = {
    Api.async.userinfo((result: ApiResponse) => {
      if(result.success) {
        loadList
      }
      else {
        startActivityForResult(new Intent(SpiderLog.this,
          classOf[LoginActivity]), SpiderLog.ACTIVITY_LOGIN)
      }
     })
  }

  def loadList = {
    getSupportFragmentManager().findFragmentById(R.id.spiderlistfragment)
      .asInstanceOf[SpiderListFragment].loadList
  }

  override def onActivityResult(request: Int, result: Int, data: Intent) = {
    if(request == SpiderLog.ACTIVITY_LOGIN) {
      if(result != Activity.RESULT_OK) finish()
      else {
        jar.sync
        loadList
      }
    }
  }

  lazy val logoutReceiver = new BroadcastReceiver() {
    override def onReceive(c: Context, i: Intent) = {
      Api.api.loggedIn = false
      val edit = getSharedPreferences("cookiejar", Context.MODE_PRIVATE).edit() 
      edit.clear()
      edit.commit()

      startActivityForResult(new Intent(SpiderLog.this, 
        classOf[LoginActivity]),
        SpiderLog.ACTIVITY_LOGIN)
      Log.d(TAG, "woot logging out")
    }
  }
  
}
