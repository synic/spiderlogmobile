package com.synicworld.spiderlog

import com.synicworld.spiderlog.api._

object Api {
  val api = new SpiderAPI()
  val async = new AsyncSpiderAPI(api)

  def init() = {
    this.async
  }
}
