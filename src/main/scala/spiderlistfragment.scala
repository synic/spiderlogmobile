package com.synicworld.spiderlog

import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.widget.AdapterView
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.MenuInflater
import android.view.ContextMenu
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.ListView
import android.content.Intent
import android.content.Context
import android.app.Activity
import android.util.Log
import android.widget.Toast
import android.content.IntentFilter
import android.content.BroadcastReceiver

import com.synicworld.util.LoadingListView

import com.synicworld.spiderlog.api.SpiderListLoader
import com.synicworld.spiderlog.api.Spider
import com.synicworld.spiderlog.api.SpiderAPI
import com.synicworld.spiderlog.api.ApiResponse

class SpiderListFragment extends Fragment 
  with LoaderManager.LoaderCallbacks[ApiResponse] 
  with AdapterView.OnItemClickListener {
  // constants
  val TAG = "SpiderListFragment"
  val LOADER_LIST = 0

  // instance members
  lazy val model = new SpiderModel(getActivity()) 
  var list: LoadingListView = null

  override def onCreate(icicle: Bundle) = {
    super.onCreate(icicle)
    setHasOptionsMenu(true)
  }

  override def onCreateView(inflater: LayoutInflater,
    container: ViewGroup, icicle: Bundle): View = {
   
    val v = inflater.inflate(R.layout.spiderlist, container, false)
    list = v.findViewById(R.id.spiderlist).asInstanceOf[LoadingListView]
    list.setAdapter(model) 

    registerForContextMenu(list)
    return v
  }

  override def onActivityCreated(bundle: Bundle) = {
    super.onActivityCreated(bundle)
    getActivity().registerReceiver(logoutReceiver, 
      new IntentFilter(SpiderLog.ACTION_LOGOUT))
  }
  
  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) = {
    inflater.inflate(R.menu.spiderlist_options, menu)
    super.onCreateOptionsMenu(menu, inflater)
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId() match {
      case R.id.refresh => {
        loading = true
        model.clear()
        getLoaderManager().restartLoader(
          LOADER_LIST, null, this)
        true
      }
      case _ => {}
    }

    return super.onOptionsItemSelected(item)
  }

  override def onCreateContextMenu(menu: ContextMenu, v: View, 
    menuInfo: ContextMenu.ContextMenuInfo) {
    new MenuInflater(getActivity().getApplication()).inflate(
      R.menu.main_context, menu)
  }

  override def onItemClick(parent: AdapterView[_], v: View, position: Int,
    i: Long): Unit = {
  }


  override def onContextItemSelected(item: MenuItem): Boolean = {
    val info = item.getMenuInfo().asInstanceOf[AdapterContextMenuInfo]
    val spider = model.getItem(info.position)

    item.getItemId match {
      case R.id.feed => addlog(spider, SpiderAPI.LOG_FEED)
      case R.id.molt => addlog(spider, SpiderAPI.LOG_MOLT)
      case R.id.premolt => addlog(spider, SpiderAPI.LOG_PREMOLT)
    }

    return super.onContextItemSelected(item)
  } 
  
  def loadList() = {
    loading = true
    getLoaderManager().initLoader(LOADER_LIST, null, this) 
  }

  override def onCreateLoader(id: Int, args: Bundle): Loader[ApiResponse] = {
    id match {
      case LOADER_LIST => new SpiderListLoader(getActivity(), Api.api)
      case _ => new SpiderListLoader(getActivity(), Api.api)
    }
  }

  override def onLoadFinished(loader: Loader[ApiResponse], 
    data: ApiResponse) = {
    loading = false
  
    if(loader.getClass == classOf[SpiderListLoader]) {
      data.data match {
        case Some(x) => {
          model.clear()
          x.asInstanceOf[List[Spider]].map {model.add(_)}
          Log.d(TAG, "SUCCESS!")
        }
        case _ => { }
      }
    }
    else {
      model.notifyDataSetChanged()
    }
  }

  def loading = false
  def loading_=(l: Boolean) = {
    getActivity().setProgressBarIndeterminateVisibility(l)
    list.setLoading(l)
  }

  override def onLoaderReset(loader: Loader[ApiResponse]) = {}

  def addlog(spider: Spider, t: String): Boolean = {
    Api.async.addlogitem(spider, t, (result: ApiResponse) => {
      val s = if(result.success) R.string.log_success else
        R.string.connection_error
      Toast.makeText(getActivity().getApplicationContext(),
        getString(s), 3000).show()
      model.notifyDataSetChanged()       
    })
    true
  }

  lazy val logoutReceiver = new BroadcastReceiver() {
    override def onReceive(c: Context, i: Intent) = {
      model.clear()

      Log.d(TAG, "logout was called")
    }
  }
}
