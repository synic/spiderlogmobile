package com.synicworld.spiderlog.api

import android.support.v4.content.AsyncTaskLoader
import android.content.Context
import android.util.Log

case class ApiResponse(success: Boolean, data: Option[Any] = None, error:
  Option[Exception] = None)

abstract class BaseLoader(context: Context, api: SpiderAPI) extends
  AsyncTaskLoader[ApiResponse](context) {
  val TAG = "BaseLoader"

  override def onStartLoading = forceLoad

  override def loadInBackground(): ApiResponse = {
    try {
      val (success, data, error) = loadData
      return new ApiResponse(success, data, error)
    }
    catch {
      case e: Exception => {
        return new ApiResponse(false, None, Some(e))
      }
    }
  }

  def loadData: Tuple3[Boolean, Option[Any], Option[Exception]]
}
