package com.synicworld.spiderlog.api

import android.support.v4.content.AsyncTaskLoader
import android.content.Context
import android.util.Log


class SpiderListLoader(context: Context, api: SpiderAPI) 
  extends BaseLoader(context, api) {
  override val TAG = "SpiderListLoader"

  def loadData: Tuple3[Boolean, Option[Any], Option[Exception]] = {
    (true, Some(api.spiderlist()), None)
  }
}
