package com.synicworld.spiderlog.api

import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import android.util.Log

import java.text.SimpleDateFormat
import java.util.Date

class Spider(var id: BigInt, var name: String, var species: String) {
  val TAG = "Spider"
  var age = ""
  var description = ""
  var hidden = false
  var sex = "u"
  var lastFed: Date = null
  var lastMolt: Date = null
  var lastPremolt: Date = null

  implicit val formats = new DefaultFormats { 
    override def dateFormatter = 
      new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  }

  def this(json: JValue) = {
    this(0, "", "")

    id = ((json \\ "id").extract[String]).toInt
    name = (json \\ "name").extract[String]
    species = (json \\ "species_name").extract[String]
    age = (json \\ "age").extract[String]
    description = (json \\ "description").extract[String]
    hidden = (json \\ "hidden").extract[Boolean]
    sex = (json \\ "sex").extract[String]
    lastFed = (json \\ "last_fed").extract[Date]
    lastMolt = (json \\ "last_molt").extract[Date]
    lastPremolt = (json \\ "last_premolt").extract[Date]
  }

  def lastFedDays: Int = {
    if(lastFed == null) return -1

    val now = new Date()
    ((now.getTime() - lastFed.getTime()) / (1000 * 60 * 60 * 24)).toInt
  }

  def lastMoltDays: Int = {
    if(lastMolt == null) return -1
    val now = new Date()
    ((now.getTime() - lastMolt.getTime()) / (1000 * 60 * 60 * 24)).toInt
  }

  def needsFeeding: Boolean = lastFedDays >= 14
  def recentlyMolted: Boolean = {
    val d = lastMoltDays
    (d != -1 && d < 14)
  }

  def premolt: Boolean = {
    if(null != lastMolt && null != lastPremolt) {
      return lastPremolt.getTime() > lastMolt.getTime()
    }
    false
  }

  def status: List[Boolean] = 
    List[Boolean](needsFeeding, recentlyMolted, premolt) 
}
