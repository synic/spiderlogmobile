package com.synicworld.spiderlog.api

import net.liftweb.json._
import net.liftweb.json.JsonDSL._

class UserInfo(var userId: BigInt, var username: String) {
  implicit val formats = DefaultFormats
  def this(json: JValue) = {
    this(0, "")

    userId = ((json \\ "user_id").extract[String]).toInt
    username = (json \\ "username").extract[String]
  }
}
