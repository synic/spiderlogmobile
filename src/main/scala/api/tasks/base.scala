package com.synicworld.spiderlog.api


abstract class BaseTask(api: SpiderAPI, f: ApiResponse => Unit) 
  extends Runnable {
  val TAG = "BaseTask"

  override def run = {
    try {
      val (success, data, error) = loadData
      
      api.handler.post(new Runnable {
        override def run() = {
          f(new ApiResponse(success, data, error))
        }
      })
    }
    catch {
      case e: Exception => {
        api.handler.post(new Runnable {
          override def run() = {
            f(new ApiResponse(false, None, Some(e)))
          }
        })
      }
    }
  }

  def loadData: Tuple3[Boolean, Option[Any], Option[Exception]]
}
