package com.synicworld.spiderlog.api

import android.util.Log

class UserInfoTask(api: SpiderAPI, f: ApiResponse => Unit) 
  extends BaseTask(api, f) {
  override val TAG = "UserInfoTask"

  override def loadData: 
    Tuple3[Boolean, Option[Any], Option[Exception]] = {
    Log.d(TAG, "loading userinfo")
    val result = api.userinfo()
    return (result != null, Some(result), None)
  }
}
