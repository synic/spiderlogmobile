package com.synicworld.spiderlog.api

class LoginTask(api: SpiderAPI, username: String, password: String,
  f: ApiResponse => Unit) extends BaseTask(api, f) {

  override def loadData:
    Tuple3[Boolean, Option[Any], Option[Exception]] = {
    (api.login(username, password), None, None)
  }
}
