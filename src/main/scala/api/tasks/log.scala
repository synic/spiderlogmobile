package com.synicworld.spiderlog.api

class LogTask(api: SpiderAPI, s: Spider, t: String, f: ApiResponse => Unit) 
  extends BaseTask(api, f) {
  override def loadData: Tuple3[Boolean, Option[Any], Option[Exception]] = {
    val result = api.addlogitem(s, t)
    return (result, Some(result), None)
  }
}
