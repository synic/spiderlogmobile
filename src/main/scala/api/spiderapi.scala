package com.synicworld.spiderlog.api

import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.io.PrintWriter
import java.io.InputStream
import java.util.Date
import scala.io.Source
import net.liftweb.json._
import net.liftweb.json.JsonDSL._

import android.util.Log
import android.os.Handler
import scala.collection.mutable.ListBuffer

class ConnectionError extends Exception

object SpiderAPI {
  val SERVER = "http://www.spiderlog.org/api/"
  val LOG_FEED = "FED"
  val LOG_MOLT = "MOL"
  val LOG_PREMOLT = "PRE"
}

class SpiderAPI {
  // constants
  val TAG = "SpiderAPI"

  // instance variables
  var username = ""
  var userId: BigInt = 0
  var loggedIn = false
  val handler = new Handler()
  implicit val formats = DefaultFormats
  
  def login(username: String, password: String): Boolean = {
    val data = ("username" -> username) ~ ("password" -> password)
    val response = connectionPOST("login", data)
    val result = (response \\ "success").extract[Boolean]
    if(result) {
      this.username = (response \\ "data" \\ 
        "username").extract[String]
      
      userId = (response \\ "data" \\ "user_id").extract[BigInt]
      
      loggedIn = true
    }

    return result
  }

  def userinfo(): UserInfo = {
    val response = connectionGET("userinfo")
    val result = (response \\ "success").extract[Boolean]
    if(result) {
      val info = new UserInfo((response \\ "data"))
      userId = info.userId
      username = info.username
      return info
    }
    return null
  }

  def addlogitem(spider: Spider, t: String = SpiderAPI.LOG_FEED): Boolean = {
    if(null == spider) return false
    val response = connectionGET("addlogitem/" + spider.id + "/" + t)
    val result = (response \\ "success").extract[Boolean]
    if(result) {
      if(t == SpiderAPI.LOG_FEED) spider.lastFed = new Date()
      else if(t == SpiderAPI.LOG_MOLT) spider.lastMolt = new Date()
      else if(t == SpiderAPI.LOG_PREMOLT) spider.lastPremolt = new Date()
    }
    return result
  }

  def spiderlist(): List[Spider] = {
    val response = connectionGET("v1/collection/spider/?user__id=" +
      userId)
    val items = new ListBuffer[Spider]
    for(item <- (response \\ "objects").children) {
      items.append(new Spider(item))
    }
    return items.toList
  }

  def connectionPOST(url: String, data: JValue): JValue = _connect(url, data)
  def connectionGET(url: String): JValue = _connect(url)

  private def _connect(url: String, data: JValue = null): JValue = {
    // get HTTP connection
    var u = SpiderAPI.SERVER + url
    if(u.indexOf("?") == -1) u += "/"
    val path = new URL(u)
    Log.d(TAG, path.toString)
    val con = path.openConnection().asInstanceOf[HttpURLConnection]
    con.setDoInput(true)

    if(data != null) {
      con.setRequestMethod("POST")
      con.setDoOutput(true)

      // write the json object to the data output stream
      val ostream = new PrintWriter(con.getOutputStream())
      ostream.print(compact(render(data)))
      ostream.close()
    }
    else {
      con.setRequestMethod("GET")
    }

    Log.d(TAG, con.getResponseCode().toString)
    if(con.getResponseCode() >= 300) {
      throw new ConnectionError
    }

    // get the response and convert it to a json object
    val contents = con.getInputStream()
    val string = Source.fromInputStream(contents).getLines.mkString
    Log.d(TAG, string)
    con.disconnect()
    Log.d(TAG, "before json parsing")
    val result = parse(string)
    Log.d(TAG, "after json parsing")
    return result
  }
}
