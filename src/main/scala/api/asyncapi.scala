package com.synicworld.spiderlog.api

import java.lang.Thread

class AsyncSpiderAPI(api: SpiderAPI) {
  def login(username: String, password: String, f: ApiResponse => Unit) = {
    runTask(new LoginTask(api, username, password, f))
  }

  def addlogitem(spider: Spider, t: String, f: ApiResponse => Unit) = {
    runTask(new LogTask(api, spider, t, f))
  }

  def userinfo(f: ApiResponse => Unit) = {
    runTask(new UserInfoTask(api, f))
  }

  private def runTask(task: BaseTask) = {
    new Thread(task).start()
  }
}
