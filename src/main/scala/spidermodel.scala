package com.synicworld.spiderlog

import android.app.Activity
import android.widget.ArrayAdapter
import android.view.View
import android.widget.TextView
import android.view.ViewGroup
import android.util.Log

import com.synicworld.spiderlog.api.Spider

import AndroidConversions._

class SpiderModel(context: Activity) extends 
  ArrayAdapter[Spider](context, R.layout.spider_row) {
  val TAG = "SpiderModel"
  lazy val inflater = context.getLayoutInflater()
  lazy val resources = context.getResources()
  val statusItems = List[Int](R.id.needs_fed,
    R.id.recently_molted, R.id.premolt)

  override def getView(position: Int, convertView: View,
    parent: ViewGroup): View = {
    
    var row = convertView
    if(row == null) {
      row = inflater.inflate(R.layout.spider_row, parent, false)
    }

    val spider = getItem(position)
    val species = row.findView[TextView](R.id.species)
    val name = row.findView[TextView](R.id.name)

    species.setText(spider.species)
    name.setText(spider.name)

    val status = spider.status
    for(i <- 0 until statusItems.length) {
      val v = row.findView[View](statusItems(i))
      v.setVisibility(
        if(status(i)) View.VISIBLE else View.GONE)
    }

    return row
  }
}
