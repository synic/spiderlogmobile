package com.synicworld.spiderlog

import java.net.HttpCookie
import java.net.CookieStore
import java.net.URI
import java.net.CookieManager
import java.lang.Thread
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream

import android.content.Context
import android.util.Log


import scala.collection.JavaConversions
import scala.collection.mutable.ListBuffer

import net.liftweb.json._
import net.liftweb.json.JsonDSL._

class CookieJar(context: Context) extends CookieStore with Runnable {
  val store = new CookieManager().getCookieStore()
  val prefs = context.getSharedPreferences("cookiejar", 
    Context.MODE_PRIVATE)
  val TAG = "CookieJar"
  
  read

  Runtime.getRuntime().addShutdownHook(new Thread(this))

  def run = write
  def add(uri: URI, cookie: HttpCookie) = store.add(uri, cookie)
  def get(uri: URI): java.util.List[HttpCookie] = store.get(uri)
  def getCookies(): java.util.List[HttpCookie] = store.getCookies()
  def getURIs(): java.util.List[URI] = store.getURIs()
  def remove(uri: URI, cookie: HttpCookie): Boolean = {
    store.remove(uri, cookie)
  }
  def removeAll() = store.removeAll()
  def sync = new Thread(this).start()

  private def write = {
    prefs synchronized {
      val editor = prefs.edit()
      editor.clear()

      val uris = JavaConversions.asScalaBuffer(store.getURIs())
      editor.putInt("num_uris", uris.length)
      
      for(i <- 0 until uris.length) {
        val uri = uris(i)
        editor.putString("uri_" + i, uri.toString)

        var cookies = JavaConversions.asScalaBuffer(store.get(uri))
        editor.putInt("uri_num_cookies_" + i, cookies.length)

        for(a <- 0 until cookies.length) {
          val c = new Cookie()
          c.setCookie(cookies(a))
          editor.putString("uri_" + i + "_cookie_" + a, c.json)
        }

      }
      editor.commit()
    }
  }

  private def read = {
    store.removeAll()
    val num = prefs.getInt("num_uris", 0)
    for(i <- 0 until num) {
      var uri = prefs.getString("uri_" + i, "")
      var numCookies = prefs.getInt("uri_num_cookies_" + i, 0)

      for(a <- 0 until numCookies) {
        var cookie = Cookie.fromJson(
          prefs.getString("uri_" + i + "_cookie_" + a, ""))
        if(cookie != null) {
          store.add(new URI(uri), cookie.getCookie)
        }
      } 
    }
  }
}

object Cookie {
  implicit val formats = DefaultFormats
  def fromJson(data: String): Cookie = {
    val json = parse(data)
    val c = new Cookie()
    c.name = (json \\ "name").extract[String]
    c.value = (json \\ "value").extract[String]
    c.comment = (json \\ "comment").extract[String]
    c.domain = (json \\ "domain").extract[String]
    c.path = (json \\ "path").extract[String]
    c.secure = (json \\ "secure").extract[Boolean]
    return c
  }
}

class Cookie {
  var name = ""
  var value = ""
  var comment = ""
  var domain = ""
  var path = ""
  var secure = false

  def json: String = {
    return compact(render(
      ("name" -> name) ~
      ("value" -> value) ~
      ("comment" -> comment) ~
      ("domain" -> domain) ~
      ("path" -> path) ~
      ("secure" -> secure)
    ))
  }

  def setCookie(cookie: HttpCookie) = {
    name = cookie.getName()
    value = cookie.getValue()
    comment = cookie.getComment()
    domain = cookie.getDomain()
    path = cookie.getPath()
    secure = cookie.getSecure()
  }

  def getCookie: HttpCookie = {
    val cookie = new HttpCookie(name, value)
    cookie.setComment(comment)
    cookie.setDomain(domain)
    cookie.setPath(path)
    cookie.setSecure(secure)
    return cookie
  }
}
