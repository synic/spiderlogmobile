package com.synicworld.spiderlog

import android.os.Bundle
import android.app.Activity
import android.util.Log
import android.support.v4.app.FragmentActivity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import com.synicworld.spiderlog.api.SpiderAPI
import com.synicworld.spiderlog.api.ApiResponse
import com.synicworld.spiderlog.api.LoginTask

import AndroidConversions._

class LoginActivity extends FragmentActivity with TypedActivity {
  val TAG = "LoginActivity"
  lazy val button = findView(TR.login_button)
  lazy val username = findView(TR.username)
  lazy val password = findView(TR.password)
  
  override def onCreate(bundle: Bundle) {
    super.onCreate(bundle)

    setContentView(R.layout.login)

    button.setOnClickListener(() => { login })
  }

  private def login(): Unit = {
    if(username.getText().isEmpty) {
      username.requestFocus()
      return
    }

    if(password.getText().isEmpty) {
      password.requestFocus()
      return
    }
    
    Api.async.login(username.getText(), password.getText(),
      (data: ApiResponse) => {
        if(data.success) {
          Toast.makeText(LoginActivity.this, 
            getString(R.string.login_successful),
            2000).show()

          setResult(Activity.RESULT_OK)
          finish()
        }
        else {
          var msg = data.error match {
            case Some(x) => x.getMessage()
            case None => getString(R.string.bad_login)
          }
          Toast.makeText(LoginActivity.this, msg, 4000).show()
        }
      })
  }
}
