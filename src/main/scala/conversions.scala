/**
 * Copyright 2012 Adam Olsen <arolsen@gmail.com>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.synicworld.spiderlog

import android.app.Activity
import android.app.NotificationManager
import android.app.AlarmManager
import android.app.KeyguardManager
import android.content.Intent
import android.content.Context
import android.content.BroadcastReceiver
import android.content.res.Configuration
import android.os.AsyncTask
import android.os.Build
import android.os.Looper
import android.os.Vibrator
import android.os.PowerManager
import android.view.LayoutInflater
import android.view.View
import android.view.KeyEvent
import android.view.MotionEvent
import android.widget.AdapterView
import android.widget.TextView
import android.widget.CheckBox
import android.content.DialogInterface
import android.telephony.TelephonyManager
import android.preference.Preference.OnPreferenceClickListener
import android.preference.Preference

trait AOActivity {
  this: Activity =>

  def findView[A](id: Int): A = {
    return findViewById(id).asInstanceOf[A]
  }
}

object AndroidConversions {
  implicit def toBroadcastReceiver(f: (Context, Intent) => Unit) =
      new BroadcastReceiver() {
    def onReceive(c: Context, i: Intent) = f(c, i)
  }

  implicit def toViewOnClickListener1(f: () => Unit) =
      new View.OnClickListener() { def onClick(v: View) = f() }
  implicit def toViewOnClickListener(f: View => Unit) =
      new View.OnClickListener() { def onClick(v: View) = f(v) }

  implicit def toDialogInterfaceOnClickListener(
      f: (DialogInterface, Int) => Unit) = {
    new DialogInterface.OnClickListener() {
      def onClick(d: DialogInterface, id: Int) = f(d, id)
    }
  }

  implicit def toDialogInterfaceOnClickListener1(f: () => Unit) = {
    new DialogInterface.OnClickListener() {
      def onClick(d: DialogInterface, id: Int) = f()
    }
  }

//  implicit def toDialogInterfaceOnShowListener(f: () => Unit) = {
//    new DialogInterface.OnShowListener() {
//      def onShow(d: DialogInterface) = f()
//    }
//  }

  implicit def toPreferenceOnClickListener(f: () => Boolean) = {
    new OnPreferenceClickListener() {
      def onPreferenceClick(p: Preference): Boolean = f()
    }
  }

  implicit def toPreferenceOnClickListener1(f: (Preference) => Boolean) = {
    new OnPreferenceClickListener() {
      def onPreferenceClick(p: Preference): Boolean = f(p)
    }
  }

  implicit def toAdapterViewOnItemClickListener(
      f: (AdapterView[_], View, Int, Long) => Unit) =
          new AdapterView.OnItemClickListener() {
        def onItemClick(
            av: AdapterView[_], v: View, pos: Int, id: Long) =
              f(av, v, pos, id)
  }

  implicit def toViewOnKeyListener(f: (View, Int, KeyEvent) => Boolean) =
          new View.OnKeyListener() {
        def onKey(v: View, key: Int, e: KeyEvent) = f(v, key, e)
  }
  implicit def toViewOnTouchListener(f: (View, MotionEvent) => Boolean) =
          new View.OnTouchListener() {
        def onTouch(v: View, e: MotionEvent) = f(v, e)
  }

  implicit def toTextViewOnEditorAction(f: (View, Int, KeyEvent) => Boolean) =
      new TextView.OnEditorActionListener() {
    def onEditorAction(v: TextView, action: Int, e: KeyEvent) =
      f(v, action, e)
  }

  implicit def toRunnable(f: () => Unit) = new Runnable() { def run() = f() }

  def async(task: AsyncTask[_,_,_]) {
    task.execute()
  }
  def async(r: Runnable) = _threadpool.execute(r)
  // ok, param: => T can only be used if called directly, no implicits
  def async(f: => Unit): Unit = async(byNameToRunnable(f))
  private def byNameToRunnable(f: => Unit) = new Runnable() { def run() = f }

  implicit def toUncaughtExceptionHandler(f: (Thread, Throwable) => Unit) =
      new Thread.UncaughtExceptionHandler {
    override def uncaughtException(t: Thread, e: Throwable) = f(t, e)
  }

  implicit def toString(c: CharSequence) =
      if (c == null) null else c.toString()
  implicit def toString(t: TextView): String = t.getText()
  implicit def toInt(t: TextView) = {
    val s: String = t.getText()
    if (s == null || s == "") -1
    else Integer.parseInt(s)
  }
  implicit def toBoolean(c: CheckBox) = c.isChecked()

  implicit def toRichView(v: View) = new RichView(v)
  implicit def toRichContext(c: Context) = new RichContext(c)
  implicit def toRichActivity(a: Activity) = new RichActivity(a)

  lazy val _threadpool = {
    import java.util.concurrent._
    import java.util.concurrent.atomic._
    // initial, max, keep-alive time
    new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS,
        new LinkedBlockingQueue[Runnable](10),
        new ThreadFactory() {
          val count = new AtomicInteger(1)
          override def newThread(r: Runnable) =
              new Thread(r,
                  "AsyncPool #" + count.getAndIncrement)
        }
    )
  }
  def isMainThread = Looper.getMainLooper.getThread == currentThread
}

case class SystemService[T](name: String)
object SystemService {
  implicit val layoutInflaterService =
      SystemService[LayoutInflater](Context.LAYOUT_INFLATER_SERVICE)
  implicit val notificationService =
      SystemService[NotificationManager](Context.NOTIFICATION_SERVICE)
  implicit val alarmService = 
    SystemService[AlarmManager](Context.ALARM_SERVICE)
  implicit val keyguardService = 
    SystemService[KeyguardManager](Context.KEYGUARD_SERVICE)
  implicit val vibratorService =
    SystemService[Vibrator](Context.VIBRATOR_SERVICE)
  implicit val telephonyService = 
    SystemService[TelephonyManager](Context.TELEPHONY_SERVICE)
  implicit val powerService = 
    SystemService[PowerManager](Context.POWER_SERVICE)

}

class RichContext(context: Context) {
  def systemService[T](implicit s: SystemService[T]): T =
      context.getSystemService(s.name).asInstanceOf[T]
}
class RichView(view: View) {
  import AndroidConversions._
  def findView[T](id: Int): T = view.findViewById(id).asInstanceOf[T]
  def onClick_= (f: => Unit) = view.setOnClickListener { () => f }
}
class RichActivity(activity: Activity) extends RichContext(activity) {
  lazy val config = activity.getResources.getConfiguration

  def findView[T](id: Int): T = activity.findViewById(id).asInstanceOf[T]
}
