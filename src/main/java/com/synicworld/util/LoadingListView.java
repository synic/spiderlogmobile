package com.synicworld.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.synicworld.spiderlog.R;

/**
 * A ListView that can show a "loading" row while information is being loaded
 * from a url, and also, show a blank row at the bottom.
 * 
 * @author Adam Olsen
 *
 */
public class LoadingListView extends ListView {
    private ArrayList<FixedViewInfo> mHeaderViewInfos = 
    	new ArrayList<FixedViewInfo>();
    private ArrayList<FixedViewInfo> mFooterViewInfos = 
    	new ArrayList<FixedViewInfo>();
    private HeaderViewListAdapter mAdapter;
    private BaseAdapter mWrapped;
	private WeakReference<LinearLayout> mHeader;
	private ViewGroup mEmptyRow;
	private boolean mShowBlankRow = false;
	private WeakReference<LinearLayout> mBlankRow;
	private boolean mInitiallyLoading = true;
	
	public LoadingListView(Context context) {
		super(context);
		initView();
	}
	
	public LoadingListView(Context context, AttributeSet set)
	{
		this(context, set, 0);
		initView();
	}
	
	public LoadingListView(Context context, 
			AttributeSet set, int defStyle) {
		super(context, set, defStyle);
		
		TypedArray a = context.obtainStyledAttributes(set, 
				R.styleable.LoadingListView);
		mShowBlankRow = a.getBoolean(
				R.styleable.LoadingListView_showBlankRow, false);
		mInitiallyLoading = a.getBoolean(
				R.styleable.LoadingListView_initiallyLoading, true);
		
		/*
		 * ALWAYS show the blank row before the Adapter is set, and 
		 * then remove it if the user specified `false` for showBlankRow.
		 * 
		 * This will cause the internal ListView to create a 
		 * HeaderViewListAdapter for list ListView, allowing 
		 * us to add and remove header and footer
		 * views without getting an exception.
		 */		
		
		if(mShowBlankRow) addBlankRow();		
		
		initView();
	}
	
	private void initView() {
		setDrawSelectorOnTop(true);
		setFooterDividersEnabled(false);
		setHeaderDividersEnabled(false);
		
		if(mInitiallyLoading) setLoading(true);
	}
	
	public void setAdapter(BaseAdapter adapter) {
		if(adapter != null) {
			mWrapped = adapter;
			mAdapter = new HeaderViewListAdapter(mHeaderViewInfos, 
					mFooterViewInfos, adapter);
			super.setAdapter(mAdapter);
		}
		else {
			mAdapter = null;
			mWrapped = null;
		}
	}
	
	public void removeBlankRow() {
		if(mBlankRow != null) {
			try {
				removeFooterView(mBlankRow.get());
			}
			catch(NullPointerException ne) { }
		}
	}
	
	public void addBlankRow() {
		if(mBlankRow == null) {
			LayoutInflater inflater = 
				((Activity)getContext()).getLayoutInflater();
			mBlankRow = new WeakReference<LinearLayout>(
					(LinearLayout)inflater.inflate(
					R.layout.blank_row, this, false));
		}
		addFooterView(mBlankRow.get(), null, false);
	}
	
	public void setLoading(boolean l) {
		if(l) {
			setEmptyText(null);
			if(mHeader == null) {
				LayoutInflater inflater = 
					((Activity)getContext()).getLayoutInflater();
				mHeader = new WeakReference<LinearLayout>(
						(LinearLayout)inflater.inflate(
						R.layout.loading_row, this, false));
				addHeaderView(mHeader.get(), null, false);
			}
		}
		else {
			if(mHeader != null) {
				try {
					removeHeaderView(mHeader.get());
				}
				catch(NullPointerException ne) { }
				mHeader = null;
			}
		}
	}
	
	public void setEmptyTextResource(Integer id) {
		Resources res = getContext().getResources();
		setEmptyText(id != null ? res.getString(id) : null);
	}
	
	public void setEmptyText(String message) {
		if(message != null) {
			if(mEmptyRow == null) {
				LayoutInflater inflater = 
					((Activity)getContext()).getLayoutInflater();
				mEmptyRow = (ViewGroup)inflater.inflate(
					R.layout.list_empty_row, this, false);
				addHeaderView(mEmptyRow, null, false);
			}
			
			TextView text = (TextView)mEmptyRow.findViewById(R.id.text);
			text.setText(message);
		}
		else {
			if(mEmptyRow != null) {
				try {
					removeHeaderView(mEmptyRow);
				}
				catch(NullPointerException ne) { }
				mEmptyRow = null;
			}
		}
	}
	
    public void addHeaderView(View v, Object data, 
    		boolean isSelectable) {
        FixedViewInfo info = new FixedViewInfo();
        info.view = v;
        info.data = data;
        info.isSelectable = isSelectable;
        mHeaderViewInfos.add(info);
        notifyChange();
    }

 
    public void addHeaderView(View v) {
        addHeaderView(v, null, true);
    }

    @Override
    public int getHeaderViewsCount() {
        return mHeaderViewInfos.size();
    }

    public boolean removeHeaderView(View v) {
        if (mHeaderViewInfos.size() > 0) {
            boolean result = false;
            if (mAdapter.removeHeader(v)) {
                result = true;
                notifyChange();
            }
            removeFixedViewInfo(v, mHeaderViewInfos);
            return result;
        }
        return false;
    }

    private void removeFixedViewInfo(View v, 
    		ArrayList<FixedViewInfo> where) {
        int len = where.size();
        for (int i = 0; i < len; ++i) {
            FixedViewInfo info = where.get(i);
            if (info.view == v) {
                where.remove(i);
                break;
            }
        }
    }

    public void addFooterView(View v, Object data, 
    		boolean isSelectable) {
        FixedViewInfo info = new FixedViewInfo();
        info.view = v;
        info.data = data;
        info.isSelectable = isSelectable;
        mFooterViewInfos.add(info);
        notifyChange();
    }

    public void addFooterView(View v) {
        addFooterView(v, null, true);
    }

    @Override
    public int getFooterViewsCount() {
        return mFooterViewInfos.size();
    }

    public boolean removeFooterView(View v) {
        if (mFooterViewInfos.size() > 0) {
            boolean result = false;
            if (mAdapter.removeFooter(v)) {
                result = true;
                notifyChange();
            }
            removeFixedViewInfo(v, mFooterViewInfos);
            return result;
        }
        return false;
    }	
    
    private void notifyChange() {
    	if(mWrapped != null) mWrapped.notifyDataSetChanged();
    }
}
